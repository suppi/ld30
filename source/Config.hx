package;

class Config
{

    public static inline var SCALE = 1.5;

    public static inline var ENTITY_SPEED = 250;
    public static inline var ENTITY_JUMPSPEED = 0;
    public static inline var ENTITY_MAXVELOCITY_X = 200;
    public static inline var ENTITY_MAXVELOCITY_Y = 500;
    public static inline var ENTITY_DRAG_SPEED = 300;
    public static inline var ENTITY_WIDTH = 32;
    public static inline var ENTITY_HEIGHT = 32;
    public static inline var ENTITY_OFFSET_W = 2;
    public static inline var ENTITY_OFFSET_H = 2;

    public static inline var PLAYER_SPEED = 400;
    public static inline var PLAYER_JUMPSPEED = 350;
    public static inline var PLAYER_MAXVELOCITY_X = 150;
    public static inline var PLAYER_MAXVELOCITY_Y = 600;
    public static inline var PLAYER_DRAG_SPEED = 350;
    public static inline var PLAYER_GRAPHICS = "assets/images/chicken3.png";
    public static inline var PLAYER_WIDTH = 32;
    public static inline var PLAYER_HEIGHT = 32;
    public static inline var PLAYER_OFFSET_W = 2;
    public static inline var PLAYER_OFFSET_H = 2;
    public static inline var PLAYER_FRAME_RATE = 6;

    public static inline var EXIT_GRAPHICS = "assets/images/exit.png";
    public static inline var LEVER_GRAPHICS = "assets/images/lever.png";

    public static inline var GATE_GRAPHICS = "assets/images/gate.png";
    public static inline var GATE_WIDTH = 64;
    public static inline var GATE_HEIGHT = 128;
    public static inline var GATE_OFFSET_W = 6;
    public static inline var GATE_OFFSET_H = 6;


    public static inline var ENEMY_JUMPSPEED = 350;
    public static inline var ENEMY_MAXVELOCITY_X = 150;
    public static inline var ENEMY_MAXVELOCITY_Y = 600;
    public static inline var ENEMY_DRAG_SPEED = 450;
    public static inline var ENEMY_GRAPHICS = "assets/images/chicken.png";
    public static inline var ENEMY_WIDTH = 32;
    public static inline var ENEMY_HEIGHT = 32;
    public static inline var ENEMY_OFFSET_W = 2;
    public static inline var ENEMY_OFFSET_H = 2;
    public static inline var ENEMY_FRAME_RATE = 6;


    public static inline var GRAVITY = 600;
    public static inline var TILE_SIZE = 32;
    public static inline var BACK_SCROLL_X = 0.3;
    public static inline var BACK_SCROLL_Y = 0.3;
    
    public static inline var ELEVATOR_GRAPHICS = "assets/images/elevator.png";
    public static inline var ELEVATOR_WIDTH = 48;
    public static inline var ELEVATOR_HEIGHT = 10;
    public static inline var ELEVATOR_OFFSET_W = 2;
    public static inline var ELEVATOR_OFFSET_H = 2;
    public static inline var ELEVATOR_MAXVELOCITY_Y = 300;

    public static inline var LEVEL_PATH = "assets/data/maps/";
    public static inline var PLAYER_POSITION = "player.txt";
    public static inline var ELEVATORS_POSITIONS = "elevators.txt";
    public static inline var ENEMIES_POSITIONS = "enemies.txt";
    public static inline var GATES_POSITIONS = "gates.txt";
    public static inline var LEVERS_POSITIONS = "levers.txt";
    public static inline var EXITS_POSITIONS = "exits.txt";
    public static inline var EXITS_WHERE = "exits-where.txt";
    public static inline var LEVERS_GATES = "levers-gates.txt";
    public static inline var LEVERS_GATES_ALT = "levers-gates-alt.txt";
}

