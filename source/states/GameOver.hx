package states;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxObject;
import flixel.group.FlxTypedGroup;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxRect;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;

class GameOver extends FlxState
{
	var isFadeDone = false;

	var text : String = "Game Over";
	var textGui : FlxText;
	public function new(t : String){ text = t; super();}
	override public function create():Void
	{
		super.create();

		FlxG.camera.fade(0x000000, 1, true, function()
        {
            textGui = new FlxText(FlxG.width / 2 - 200 / 2, FlxG.height / 2 - 20, 400, text, 40);
            textGui.alignment = "center";

            add(textGui);
            FlxSpriteUtil.fadeIn(textGui, 1, true);
		});
	}
	
	override public function destroy():Void
	{
		super.destroy();
	}

	override public function update():Void
	{
		if (!isFadeDone) return;

		super.update();
	}	
}
