package states;

import Std;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxObject;
import flixel.tile.FlxTilemap;
import flixel.FlxCamera;
import flixel.group.FlxTypedGroup;
import flixel.group.FlxGroup;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxRect;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;
import flixel.util.FlxPoint;
import flixel.util.FlxStringUtil;
import openfl.Assets;

import entities.Entity;
import entities.Gate;
import entities.Lever;
import entities.Exit;

class PlayState extends FlxState
{
    var frontLayer : FlxTilemap;
    var backLayer  : FlxTilemap;

    var movementManager : MovementManager;

	var player : entities.Player;
	var elevators : FlxTypedGroup<Entity>;
	var enemies : FlxTypedGroup<Entity>;
	var gates : FlxTypedGroup<Gate>;
	var levers : FlxTypedGroup<Lever>;
	var exits : FlxTypedGroup<Exit>;

	var start_x : Int = 800;
	var start_y : Int = 400;

	var isFadeDone = false;
	var isReversed = false;
	var isEnd = false;

    var levelPath : String = "level1/";

	public function new(level_path : String)
	{
		super();
        if (level_path != null) this.levelPath = level_path + "/";
        movementManager = new MovementManager();
	}
	override public function create():Void
	{
		super.create();

		FlxG.camera.fade(0x000000, 2, true, function() {
            init();
        });

		initMap(levelPath);
        initEntities(levelPath);
    }
    function initEntities(path : String)
    { 
        path = Config.LEVEL_PATH + path;

        elevators = new FlxTypedGroup<Entity>();
        enemies = new FlxTypedGroup<Entity>();
        gates = new FlxTypedGroup<Gate>();
        levers = new FlxTypedGroup<Lever>();
        exits = new FlxTypedGroup<Exit>();
        Assets.loadText(path + Config.PLAYER_POSITION, function (text : String) { initPlayer(text2pos(text)); });
        Assets.loadText(path + Config.ELEVATORS_POSITIONS, function (text : String) { initElevators(text2pos(text)); });
        Assets.loadText(path + Config.ENEMIES_POSITIONS, function (text : String) { initEnemies(text2pos(text)); });
        Assets.loadText(path + Config.GATES_POSITIONS, function (text : String) { initGates(text2pos(text)); });
        Assets.loadText(path + Config.LEVERS_POSITIONS, function (text : String) {
            Assets.loadText(path + Config.LEVERS_GATES, function (text2 : String) {
                Assets.loadText(path + Config.LEVERS_GATES_ALT, function (text3 : String) { initLevers(text2pos(text), text2gates(text2), text2gates(text3)); });
            });
        });
        Assets.loadText(path + Config.EXITS_POSITIONS, function (text : String) {
            Assets.loadText(path + Config.EXITS_WHERE, function (text2 : String) {
                initExits(text2pos(text), text2exits(text2));
            });
        });
	}
    function text2pos(text : String) : Array<FlxPoint>
    {
        return text.split("\n").filter(function (x) { return (x != ""); })
            .map
                (function (y) 
                {
                    var arr = y.split(" ").map(function (z) { return Std.parseInt(z); });
                    return (new FlxPoint(arr[0],arr[1]));
                });
    }
    function text2gates(text : String) : Array<Array<Int>>
    {
        return text.split("\n").filter(function (x) { return (x != ""); })
            .map
                (function (y) 
                {
                    return y.split(" ").map(function (z) { return Std.parseInt(z); });
                });
    }
    function text2exits(text : String) : Array<String>
    {
        return text.split("\n").filter(function (x) { return (x != ""); });
    }



	private function initMap(loc : String)
	{
		//initBounds();
       
        backLayer = new FlxTilemap();
        backLayer.auto = FlxTilemap.AUTO;
        backLayer.loadMap(FlxStringUtil.imageToCSV("assets/data/maps/" + loc + "testback2.png"), "assets/data/maps/tile2.png", Config.TILE_SIZE, Config.TILE_SIZE, 0,0);
        backLayer.scrollFactor.x = Config.BACK_SCROLL_X;
        backLayer.scrollFactor.y = Config.BACK_SCROLL_Y;

        frontLayer = new FlxTilemap();
        frontLayer.auto = FlxTilemap.AUTO;
        frontLayer.loadMap(FlxStringUtil.imageToCSV("assets/data/maps/" + loc + "test-fore2.png"), "assets/data/maps/tiles.png", Config.TILE_SIZE, Config.TILE_SIZE, FlxTilemap.AUTO);

        this.add(backLayer);
        this.add(frontLayer);

        FlxG.worldBounds.set(0, 0, backLayer.getBounds().width, backLayer.getBounds().height);
	}
	function initElevators(positions : Array<FlxPoint>)
	{

        for (pos in positions)
        {
            var elevator = new Entity(pos.x * Config.TILE_SIZE, pos.y * Config.TILE_SIZE);
            var animap = new Map<String, Array<Int>>();
            animap.set("idle", [0]);
            animap.set("up", [0]);
            animap.set("down", [0]);
            animap.set("right", [0]);
            animap.set( "left", [0]);

            elevator.addGraphics(Config.ELEVATOR_GRAPHICS, animap, Config.ELEVATOR_WIDTH, Config.ELEVATOR_HEIGHT, Config.ELEVATOR_OFFSET_W, Config.ELEVATOR_OFFSET_H);
            elevator.type = "ELEVATOR";
            elevator.maxVelocity.y = Config.ELEVATOR_MAXVELOCITY_Y;

            elevators.add(elevator);
        }
        this.add(elevators);
	}
	function initEnemies(positions : Array<FlxPoint>)
	{
        var animap = new Map<String, Array<Int>>();
        animap.set("idle", [0]);
        animap.set("up", [2]);
        animap.set("down", [2]);
        animap.set("right", [0,1,2,3]);
        animap.set( "left", [0,1,2,3]);

        for (pos in positions)
        {
            var obj = new Entity(pos.x * Config.TILE_SIZE, pos.y * Config.TILE_SIZE);

            obj.addGraphics(Config.ENEMY_GRAPHICS, animap, Config.ENEMY_WIDTH, Config.ENEMY_HEIGHT, Config.ENEMY_OFFSET_W, Config.ENEMY_OFFSET_H);
            obj.type = "ENEMY";
            obj.maxVelocity.y = Config.ENEMY_MAXVELOCITY_Y;

            enemies.add(obj);
        }
        this.add(enemies);
	}
	function initGates(positions : Array<FlxPoint>)
	{
        var animap = new Map<String, Array<Int>>();
        animap.set(    "up", [4]);
        animap.set(  "down", [0]);
        animap.set("godown", [0,1,2,3,4]);
        animap.set(  "goup", [4,3,2,1,0]);


        var i = 0;
        for (pos in positions)
        {
            var obj = new Gate(pos.x * Config.TILE_SIZE, pos.y * Config.TILE_SIZE - Config.GATE_HEIGHT, 1);

            obj.addGraphics(Config.GATE_GRAPHICS, animap, Config.GATE_WIDTH, Config.GATE_HEIGHT, Config.GATE_OFFSET_W, Config.GATE_OFFSET_H);
            obj.type = "GATE";
            obj.num = i;
            gates.add(obj);
            i+=1;
        }
        this.add(gates);
	}
	function initLevers(positions : Array<FlxPoint>, gatesArr : Array<Array<Int>>, gatesArrAlt : Array<Array<Int>>)
	{
        var animap = new Map<String, Array<Int>>();
        animap.set(    "up", [3]);
        animap.set(  "down", [0]);
        animap.set("godown", [0,1,2,3]);
        animap.set(  "goup", [3,2,1,0]);


        var i = 0;
        for (pos in positions)
        {
            var obj = new Lever(pos.x * Config.TILE_SIZE, pos.y * Config.TILE_SIZE, 1);

            obj.addGraphics(Config.LEVER_GRAPHICS, animap, Config.TILE_SIZE, Config.TILE_SIZE, 2, 2);
            obj.type = "LEVER";
            obj.gates = gatesArr[i];
            obj.altgates = gatesArrAlt[i];
            levers.add(obj);
            i+=1;
        }
        this.add(levers);
	}
	function initExits(positions : Array<FlxPoint>, where : Array<String>)
	{

        var i = 0;
        for (pos in positions)
        {
            var obj = new Exit(pos.x * Config.TILE_SIZE, pos.y * Config.TILE_SIZE);
            var animap = new Map<String, Array<Int>>();
            animap.set("idle", [0]);
            animap.set("up", [0]);
            animap.set("down", [0]);
            animap.set("right", [0]);
            animap.set( "left", [0]);

            obj.addGraphics(Config.EXIT_GRAPHICS, animap, Config.ENTITY_WIDTH, Config.ENTITY_HEIGHT, Config.ENTITY_OFFSET_W, Config.ENTITY_OFFSET_H);
            obj.type = "EXIT";
            obj.maxVelocity.y = Config.ENTITY_MAXVELOCITY_Y;
            obj.exit = where[i];

            exits.add(obj);
            i+=1;
        }
        this.add(exits);
	}

	function init()
	{
        initCamera();
    }
function initCamera()
{
    FlxSpriteUtil.fadeIn(player, 0.5, true, function(e)
    {
        FlxG.camera.zoom = Config.SCALE;
        FlxG.camera.setSize(Std.int(FlxG.width / Config.SCALE), Std.int(FlxG.height / Config.SCALE));
        FlxG.camera.follow(player, FlxCamera.STYLE_PLATFORMER, new FlxPoint(0,0), 5);
        FlxG.camera.update();

        FlxG.sound.playMusic("music1");
        isFadeDone = true;
    });
}

	private function initPlayer(pos : Array<FlxPoint>)
	{
        var animap = new Map<String, Array<Int>>();
        animap.set("idle", [0]);
        animap.set("interact", [2]);
        animap.set("up", [0]);
        animap.set("down", [0]);
        animap.set("right", [0,1,2,3]);
        animap.set( "left", [0,1,2,3]);
		player = new entities.Player(Config.TILE_SIZE * pos[0].x, Config.TILE_SIZE * pos[0].y, animap);
		this.add(player);
	}

	private function onGameOver()
	{
		FlxG.camera.fade(0x000000, 3, false, function() { FlxG.switchState(new states.GameOver("GAME OVER")); });

	}
	private function onGameSuccess()
	{
		FlxG.camera.fade(0x000000, 4, false, function()
        {
			FlxG.switchState(new states.GameOver("END :)"));
        });
	}

	override public function destroy():Void
	{
		super.destroy();
	}

	override public function update():Void
	{
		if (!isFadeDone) return;
        if (FlxG.keys.anyPressed(["SPACE"]))
        {
            isFadeDone = false;
            FlxG.camera.flash(0xFFFFFF, 0.5, function() { isReversed = !isReversed; isFadeDone = true; }); 
        }
        manageCollisions();
        manageMovement();
        interact();
		super.update();
	}

    function manageCollisions()
    {
        // frontLayer
        if (!FlxG.keys.pressed.Q || !FlxG.keys.pressed.E) FlxG.collide(player, frontLayer);
        // elevators
        FlxG.collide(player, elevators);
        FlxG.collide(frontLayer, elevators);
        FlxG.collide(elevators, elevators);
        // enemies
        FlxG.collide(player, enemies);
        FlxG.collide(frontLayer, enemies);
        FlxG.collide(elevators, enemies);
//        FlxG.collide(enemies, enemies);
        FlxG.overlap(player, enemies, collPlayerEnemy);

        // gates
        for (gate in gates)
        {
            if (gate.direction > 0)
            {
                FlxG.collide(player, gate);
                FlxG.collide(enemies, gate);
                FlxG.collide(elevators, gate);
                FlxG.collide(frontLayer, gate);
            }
        }
        // exits
        FlxG.overlap(player, exits, exit);
    }
    function exit(player, exit : Exit)
    {
        if (exit.exit == "END")
            onGameSuccess();
    }
    function manageMovement()
    {
        movementManager.updateMovement(isReversed, player);
        for (elevator in elevators) movementManager.updateMovement(isReversed, elevator);
        for (enemy in enemies) movementManager.updateMovement(isReversed, enemy);
    }
	function collPlayerEnemy(player, enemy)
	{
        FlxG.sound.play("hit");
        enemy.velocity.x = Math.floor(2*enemy.velocity.x);
        if (enemy.direction == player.direction)
            player.velocity.x = -10*enemy.velocity.x;
        else
            player.velocity.x = 10*enemy.velocity.x;
	}
    function interact()
    {
        if (FlxG.keys.justPressed.ENTER)
        {
            player.animation.play("interact");
            FlxG.overlap(player, levers,
                function (p, lever : Lever)
                {
                    FlxG.sound.play("jump");
                    lever.direction = -lever.direction;
                    if (!isReversed)
                    {
                        for (i in lever.gates)
                            for (gate in gates)
                                if (gate.num == i) gate.direction = -gate.direction;
                    }
                    else {
                        for (i in lever.altgates)
                            for (gate in gates)
                                if (gate.num == i) gate.direction = -gate.direction;
                    }
                });

        }
    }
}
