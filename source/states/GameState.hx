package states;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxObject;
import flixel.group.FlxTypedGroup;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxRect;
import flixel.util.FlxColor;

class StartState extends FlxState
{
	var isFadeDone = false;

	override public function create():Void
	{
		super.create();

		FlxG.camera.fade(0x000000, 2, true, function() { isFadeDone = true; });
	}
	
	override public function destroy():Void
	{
		super.destroy();
	}

	override public function update():Void
	{
		if (!isFadeDone) return;

		super.update();
	}	
}
