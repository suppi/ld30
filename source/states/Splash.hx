package states;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;
import flixel.util.FlxSpriteUtil;

class Splash extends FlxState
{
	static inline var initialTime : Float = 0.05;
	var cloud1 : FlxSprite;
	var key_pressed = false;

	override public function create() : Void
	{
		var comment = new FlxText(FlxG.width / 2 - 500 / 2, FlxG.height - 150, 500, "PRESS 'SPACE' TO START", 20);
		comment.alignment = "center";
		comment.color = 0xFFFFFFFF;

		FlxG.camera.fade(0xFFFFFF, 1.5, true,
			function() {
					FlxSpriteUtil.fadeIn(comment, 1);
					this.add(comment);
			});
		super.create();
	}
	
	override public function destroy():Void
	{
		super.destroy();
	}

	override public function update():Void
	{

		if (FlxG.keys.anyPressed(["SPACE", "ENTER"]))
		{
			key_pressed = true;
			if (key_pressed)
			{
				FlxG.camera.fade(0x000000, 1, false, function () {
					FlxG.switchState(new PlayState("level1"));
				});
			}
		}

		super.update();
	}
}
