package entities;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxColor;

class Player extends Entity
{
	public function new(x : Float, y : Float, animation_map : Map<String, Array<Int>>)
	{
		super(x,y);
        speed = Config.ENTITY_SPEED;
        maxVelocity.x = Config.ENTITY_MAXVELOCITY_X;

        this.type = "PLAYER";

		this.addGraphics(Config.PLAYER_GRAPHICS, animation_map, Config.PLAYER_WIDTH, Config.PLAYER_HEIGHT, Config.PLAYER_OFFSET_W, Config.PLAYER_OFFSET_H);

        this.jumpSpeed = Config.PLAYER_JUMPSPEED;
	}
    public override function update()
    {
        super.update();
    }
}
