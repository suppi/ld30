package entities;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxColor;

class Entity extends FlxSprite
{
	public var maxVelX = Config.ENTITY_MAXVELOCITY_X;
	public var maxVelY = Config.ENTITY_MAXVELOCITY_Y;
	public var speed = Config.ENTITY_SPEED;
	public var jumpSpeed = Config.ENTITY_JUMPSPEED;
	public var dragSpeed = Config.ENTITY_DRAG_SPEED;
    public var direction = -1;

    public var type : String = "ENTITY";
	public function new(x : Float, y : Float)
	{
		super(x,y);

		this.drag.y = dragSpeed;
		this.drag.x = dragSpeed;
        this.maxVelocity.set(maxVelX, maxVelY);
        speed -= Math.floor(Math.random() * 250);
        maxVelocity.x -= Math.floor(Math.random() * 150);
        antialiasing = true;
	}
    public function addGraphics(graphics : String, animation_map : Map<String, Array<Int>>, w : Int, h : Int, ow : Int, oh : Int) : Entity
    {
        var animated = false;
        if (animation_map.keys().hasNext()) animated = true;

		this.loadGraphic(graphics, animated, w, h);

        if (!animated)
        {
            animation_map.set("idle", [0]);
            animation_map.set("up", [0]);
            animation_map.set("down", [0]);
            animation_map.set("right", [0]);
            animation_map.set( "left", [0]);
        }


        for (anime in animation_map.keys())
            this.animation.add(anime, animation_map.get(anime), Config.PLAYER_FRAME_RATE);

        this.width  = w;
		this.height = h;
		this.offset = new FlxPoint(ow, oh);

        return this;
    }


	public override function update()
	{
		super.update();
		setAnimation();
	}
	public function handleMovement(vel : FlxPoint, reversed : Bool)
	{
        this.acceleration.x = vel.x;
        this.acceleration.y = vel.y;
        if (0 < velocity.x)
        {
            if ( flipX &&  reversed) flipX = false;
            if (!flipX && !reversed) flipX = true;
        }
        if (velocity.x < 0)
        {
            if ( flipX && !reversed) flipX = false;
            if (!flipX &&  reversed) flipX = true;
        }
	}
    
	function setAnimation() : Void
	{
		if (!(velocity.x != 0 || velocity.y != 0))
			animation.play("idle");

		if (velocity.y > 0) this.animation.play("down");
		else if (velocity.y < 0) this.animation.play("up");
		if (velocity.x > 0) this.animation.play("right");
		else if (velocity.x < 0) this.animation.play("left");
	}
}
