package entities;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxColor;

class Exit extends Gate
{
    public var exit : String;
	public function new(x : Float, y : Float)
	{
		super(x,y,0);
        type = "EXIT";
	}
}
