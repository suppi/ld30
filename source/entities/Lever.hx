package entities;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxColor;

class Lever extends Gate
{
    public var gates : Array<Int>;
    public var altgates : Array<Int>;
	public function new(x : Float, y : Float, dir : Int)
	{
		super(x,y,dir);
        type = "LEVER";
	}
}
