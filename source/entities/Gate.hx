package entities;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxColor;

class Gate extends Entity
{
    public var num : Int;
	public function new(x : Float, y : Float, dir : Int)
	{
		super(x,y);

        type = "GATE";
		this.drag.y = 0;
		this.drag.x = 0;
        this.maxVelocity.set(0,0);
        speed = 0;
        maxVelocity.x = 0;
        maxVelocity.y = 0;
        direction = dir;
        immovable = true;
	}
    public override function addGraphics(graphics : String, animation_map : Map<String, Array<Int>>, w : Int, h : Int, ow : Int, oh : Int) : Entity
    {
        var animated = false;
        if (animation_map.keys().hasNext()) animated = true;
		this.loadGraphic(graphics, animated, w, h);

        for (anime in animation_map.keys())
            this.animation.add(anime, animation_map.get(anime), Config.PLAYER_FRAME_RATE, false);

        this.width  = w;
		this.height = h;
		this.offset = new FlxPoint(ow, oh);

        return this;
    }


	public override function update()
	{
		super.update();
		setAnimation();
	}
	public override function handleMovement(vel : FlxPoint, reversed : Bool)
	{
	}
    
	override function setAnimation() : Void
	{
        if (direction < 0 && animation.finished) animation.play("up");
        if (direction > 0 && animation.finished) animation.play("down");

        if ( speed > 0) animation.play("godown");
        if ( speed < 0) animation.play("goup");
	}
}
