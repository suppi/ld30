package ;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxPoint;
import flixel.group.FlxTypedGroup;
import flixel.util.FlxMath;
import flixel.util.FlxRect;
import flixel.util.FlxColor;
import entities.Entity;
import entities.Player;

class MovementManager
{
    var updateMap : Map<String, Bool -> Entity -> Void>;


    public function new()
    {
        updateMap = new Map<String, Bool -> Entity -> Void>();
        updateMap.set("ENTITY", function(a, b) { });
        updateMap.set("PLAYER", updatePlayerMovement);
        updateMap.set("ELEVATOR", updateElevatorMovement);
        updateMap.set("ENEMY", updateEnemyMovement);


    }

    public function updateMovement(reversed : Bool, entity : Entity) : Void
    {
        var updateFunction = updateMap.get(entity.type);
        if (updateFunction != null) updateFunction(reversed, entity);
        else trace("cannot move entity of type: " + entity.type);
    }
	public function updatePlayerMovement(reversed : Bool, player : Entity) : Void
	{
        var vel = new FlxPoint(0, Config.GRAVITY);

        if (FlxG.keys.anyPressed(["A", "LEFT"])) vel.x -= player.speed;
        if (FlxG.keys.anyPressed(["D", "RIGHT"])) vel.x += player.speed;
        if (FlxG.keys.anyPressed(["s", "DOWN"])) vel.y += player.speed;
        if (FlxG.keys.justPressed.W ||  FlxG.keys.justPressed.UP)
        {
            if (player.isTouching(FlxObject.FLOOR))
            {
                player.velocity.y -= player.jumpSpeed;
                vel.y = 0;
            }
        }

        if (reversed) { vel.x = -vel.x; }

        if (player.velocity.x > 0) player.direction = 1;
        else player.direction = -1;

        player.handleMovement(vel, reversed);
	}
	public function updateElevatorMovement(reversed : Bool, elevator : Entity) : Void
    {
        var vel = new FlxPoint(0, 0);
        if (elevator.justTouched(FlxObject.UP)) elevator.velocity.y = 0;
        if (elevator.isTouching(FlxObject.UP) && reversed)
        {
            vel.y = -Config.GRAVITY*2;
            FlxG.sound.play("elevator");
        }

        elevator.handleMovement(vel, reversed);

    }
	public function updateEnemyMovement(reversed : Bool, entity : Entity) : Void
    {
        var vel = new FlxPoint(0,  Config.GRAVITY);
        var dir = entity.direction;
        if ((entity.isTouching(FlxObject.LEFT) && dir < 0) || (entity.isTouching(FlxObject.RIGHT) && dir > 0)) entity.direction = -dir;
        if (entity.isTouching(FlxObject.FLOOR)) vel.x = Config.ENTITY_SPEED * dir;

        entity.handleMovement(vel, reversed);
    }
}
